extends Container

var drag_pos = null
var hour_format = "12h"
var date_format = "None"
var is_expanded = false
var expanded_margin = 240

func _ready():
	get_tree().get_root().set_transparent_background(true)

	# Load saved config settings if available
	var file = File.new()
	var file_exists = file.file_exists("res://config.res")
	var pos_x = 685
	var pos_y = 415
	if file_exists:
		file.open("res://config.res", File.READ)
		var config = str2var(file.get_as_text())
		pos_x = config["pos_x"] if "pos_x" in config else pos_x
		pos_y = config["pos_y"] if "pos_y" in config else pos_y
		hour_format = config["hour_format"] if "hour_format" in config else hour_format
		date_format = config["date_format"] if "date_format" in config else date_format
		var size = config["size"] if "size" in config else 100
		var color = config["color"] if "color" in config else Color("ffffc600")
		var outline_color = config["outline_color"] if "outline_color" in config else Color("21e9e6b2")
		var family = config["family"] if "family" in config else $ContextMenu.families["Moshimoji"]
		var font_settings = {
			"size": size,
			"color": color,
			"outline_color": outline_color,
			"family": family,
		}
		OS.set_window_position(Vector2 (pos_x,pos_y))
		load_font(font_settings)
		$DateLabel.visible = false if date_format == "None" else true
	$DateLabel.rect_position.y = $ClockLabel.get("custom_fonts/normal_font").size
	OS.set_window_position(Vector2 (pos_x,pos_y))
	position_window(false)
	OS.set_window_position(Vector2 (pos_x,pos_y))

func _process(_delta):
	# Format the time
	var time = OS.get_datetime()
	var new_time = time
	var minute = "0" + String(time.minute) if time.minute < 10 else String(time.minute)
	var second = "0" + String(time.second) if time.second < 10 else String(time.second)
	var hour = time.hour
	if hour_format == "12h":
		hour = time.hour if time.hour > 0 else 12
		hour = hour - 12 if hour > 12 else hour
	hour = " " + String(hour) if hour < 10 else String(hour)
	new_time = hour + ":" + minute + ":" + second
	$ClockLabel.bbcode_text = "[center]" + new_time + "[/center]"

	# Format the date
	if date_format != "None":
		var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
		var month_name = ["January", "February", "March", "April", "May", "June", "July", "August",
			"September", "October", "November", "December"]
		var dayofweek = time["weekday"]
		var day = time["day"]
		var month = time["month"]
		var year = time["year"] 
		var new_date = time
		var ordinal = "th"
		var last_digit = String(day).right(0)
		if last_digit == "1":
			ordinal = "st"
		elif last_digit == "2":
			ordinal = "nd"
		elif last_digit == "3":
			ordinal = "rd"
		if date_format == "day date":
			new_date = weekday[dayofweek] + " " + String(day) + ordinal
		elif date_format == "day month date":
			new_date = weekday[dayofweek] + "\n" + month_name[month - 1] + " " + String(day) + ordinal
		elif date_format == "MM/DD/YYYY":
			new_date = String(month) + "/" +  String(day) + "/" + String(year)
		elif date_format == "DD/MM/YYYY":
			new_date =  String(day) + "/" + String(month) + "/" + String(year)
		elif date_format == "YYYY/MM/DD":
			new_date = String(year) + "/" + String(month) + "/" +  String(day)
		$DateLabel.bbcode_text = "[center]" + new_date + "[/center]"

# Drag handler, Context menu opening
func _on_ClockLabel_gui_input(event):
	if event is InputEventMouseButton:
		$ContextMenu.visible = false
		if event.button_index == 1:
			if event.pressed:
				yield(get_tree().create_timer(0.02), "timeout") # Allow time for os to resize window
				drag_pos = get_global_mouse_position()
			else:
				drag_pos = null
				save_settings()
		elif !event.pressed && event.button_index == 2:
			# Open ContextMenu
			position_window(true)
			yield(get_tree().create_timer(0.02), "timeout") # Allow time for os to resize window
			# Calculate the ContextMenu position to keep it on screen
			# This is an approximation since we have no way of knowing if the resolution is
			# different on a 2nd/3rd monitor
			var x = get_global_mouse_position().x
			var y = get_global_mouse_position().y
			var current_monitor_x = int(OS.window_position.x) % int(OS.get_screen_size().x)
			var click_pos_x = current_monitor_x + get_global_mouse_position().x
			var open_to_the_left = (click_pos_x + $ContextMenu.rect_size.x) > OS.get_screen_size().x
			if open_to_the_left:
				 x -= $ContextMenu.rect_size.x
			var current_monitor_y = int(OS.window_position.y) % int(OS.get_screen_size().y)
			var click_pos_y = current_monitor_y + get_global_mouse_position().y
			var open_upwards = (click_pos_y + $ContextMenu.rect_size.y) > OS.get_screen_size().y
			if open_upwards:
				y -= $ContextMenu.rect_size.y
			$ContextMenu.set_position(Vector2(x, y))
			$ContextMenu.visible = true
	if event is InputEventMouseMotion and event.button_mask == 0:
		# Windows issue work-around to prevent dragging after closing ContextMenu
		drag_pos = null
	elif event is InputEventMouseMotion and drag_pos:
		OS.set_window_position(get_global_mouse_position() + OS.window_position - drag_pos)

func _on_DateLabel_gui_input(event):
	_on_ClockLabel_gui_input(event)

# ContextMenu needs extra room only when open
func _on_ContextMenu_hide():
	if !$ColorPickerPanel.visible:
		position_window(false)

# We have to make the window bigger when the ContextMenu is open in order to see it.
# We also have to shrink it when its closed otherwise, we take over clicks for a
# large swath of the desktop when we have focus.
func position_window(expand : bool):
	# Hide everything for a split second while we change the window size/position
	# Othwerise, the clock will jump around briefly
	$"../ClockNode".visible = false
	yield(get_tree().create_timer(0.02), "timeout")
	var font_size = $ClockLabel.get("custom_fonts/normal_font").size
	var show_date = date_format != "None"
	var new_size_x = font_size * 5
	var new_size_y = font_size * (1.15 + (1.25 * int(show_date)))
	var new_pos_x = OS.get_window_position().x
	var new_pos_y = OS.get_window_position().y
	if expand && !is_expanded:
		new_size_x += expanded_margin * 2
		new_size_y += expanded_margin *2
		new_pos_x -= expanded_margin
		new_pos_y -= expanded_margin
		is_expanded = true
		$ClockLabel.rect_position.y += expanded_margin
		$DateLabel.rect_position.y = expanded_margin + font_size
	elif !expand and is_expanded:
		new_pos_x += expanded_margin
		new_pos_y += expanded_margin
		is_expanded = false
		$ClockLabel.rect_position.y = 0
		$DateLabel.rect_position.y = font_size
	OS.set_window_size(Vector2(new_size_x, new_size_y))
	OS.set_window_position(Vector2(new_pos_x, new_pos_y))
	yield(get_tree().create_timer(0.02), "timeout")
	$"../ClockNode".visible = true

# Loads the given font settings without changing any settings that arent given
func load_font(settings : Dictionary):
	var existing_font = $ClockLabel.get("custom_fonts/normal_font")
	var size = settings["size"] if "size" in settings else existing_font.size
	var color = settings["color"] if "color" in settings else $ClockLabel.get("custom_colors/default_color")
	var outline_color = settings["outline_color"] if "outline_color" in settings else existing_font.outline_color
	var family = settings["family"] if "family" in settings else existing_font.font_data.font_path

	# Apply font settings to time
	var width = int(550 * size/100)
	var height = int(125 * size/100)
	var time_font = DynamicFont.new()
	time_font.font_data = load(family)
	time_font.outline_size = floor(size / 30)
	time_font.size = size
	time_font.set_outline_color(outline_color)
	$ClockLabel.rect_size = Vector2(width, height)
	$ClockLabel.rect_position = Vector2(width/-2, 0)
	$ClockLabel.set("custom_colors/default_color", color)
	$ClockLabel.set("custom_fonts/normal_font", time_font)

	# Apply font settings to date
	var date_font = DynamicFont.new()
	date_font.font_data = load(family)
	date_font.outline_size = floor(size / 60)
	date_font.size = int(size / 2)
	date_font.set_outline_color(outline_color)
	$DateLabel.rect_size = Vector2(width, height)
	$DateLabel.rect_position = Vector2(width/-2, height)
	$DateLabel.set("custom_colors/default_color", color)
	$DateLabel.set("custom_fonts/normal_font", date_font)
	save_settings()

func save_settings():
	var file = File.new()
	file.open("res://config.res", File.WRITE)

	# Save settings
	var current_position = OS.get_window_position()
	if is_expanded:
		current_position.x += expanded_margin
		current_position.y += expanded_margin
	var config = {
		"pos_x": current_position.x,
		"pos_y": current_position.y,
		"hour_format": hour_format,
		"size": $ClockLabel.get("custom_fonts/normal_font").size,
		"color": $ClockLabel.get("custom_colors/default_color"),
		"outline_color": $ClockLabel.get("custom_fonts/normal_font").outline_color,
		"family": $ClockLabel.get("custom_fonts/normal_font").font_data.font_path,
		"date_format": date_format,
	}

	file.store_string(var2str(config))
	file.close()

func _exit_tree():
	position_window(false)
	save_settings()
