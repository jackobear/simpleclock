extends Panel

var element = "font"

func _ready():
	pass

func _on_CancelColorPicker_pressed():
	visible = false
	$"../../ClockNode".position_window(false)

func _on_OkayColorPicker_pressed():
	if element == "font":
		$"../../ClockNode".load_font({"color": $FontColorPicker.color})
	else:
		$"../../ClockNode".load_font({"outline_color": $FontColorPicker.color})
	visible = false
	$"../../ClockNode".position_window(false)
