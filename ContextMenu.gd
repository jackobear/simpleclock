extends PopupMenu
var time_submenu = PopupMenu.new()
var font_size_submenu = PopupMenu.new()
var date_submenu = PopupMenu.new()
var font_submenu = PopupMenu.new()
# Monospace fonts only, otherwise they jump around when ticking up
var families = {
	"Droid Sans": "res://fonts/droid-sans/DroidSansMono.ttf",
	"Hack": "res://fonts/hack/HackBold-Pdjd.ttf",
	"LCD": "res://fonts/lcd/LCD14.otf",
	"Mechanical": "res://fonts/mechanical/MechanicalBold-oOmA.otf",
	"Monospace": "res://fonts/monospace/MonospaceBold-zmP0.ttf",
	"Moshimoji": "res://fonts/moshimoji/Moshimoji-0Y6R.ttf",
	"Nova": "res://fonts/novamono/Novamono-njdg.ttf",
	"Poe": "res://fonts/poe/PoeMonospace-dJJ6.ttf",
	"Quatre Quartz": "res://fonts/quatre-quarts/QuatreQuarts-61ag.otf",
	"Repitition": "res://fonts/repetition/repet___.ttf",
	"TT2020": "res://fonts/tt2020/Tt2020StyleB-axYBg.ttf",
}

func _ready():
	font_submenu.set_name("font_submenu")
	var family_names = families.keys();
	for family_name in family_names: 
		font_submenu.add_item(family_name)
	font_submenu.connect("id_pressed", self, "_on_font_pressed")
	add_child(font_submenu)
	add_submenu_item("Change Font", "font_submenu")
	
	add_item("Change Font Color")
	add_item("Change Font Outline Color")

	font_size_submenu.set_name("font_size_submenu")
	font_size_submenu.add_item("50pt")
	font_size_submenu.add_item("75pt")
	font_size_submenu.add_item("100pt")
	font_size_submenu.add_item("125pt")
	font_size_submenu.add_item("150pt")
	font_size_submenu.add_item("175pt")
	font_size_submenu.add_item("200pt")
	font_size_submenu.connect("id_pressed", self, "_on_font_size_pressed")
	add_child(font_size_submenu)
	add_submenu_item("Change Font Size", "font_size_submenu")

	date_submenu.set_name("date_submenu")
	date_submenu.add_item("Sun 31st")
	date_submenu.add_item("Sunday, December 31st")
	date_submenu.add_item("12/31/2023")
	date_submenu.add_item("31/12/2023")
	date_submenu.add_item("2023/12/31")
	date_submenu.add_item("None")
	date_submenu.connect("id_pressed", self, "_on_date_format_pressed")
	add_child(date_submenu)
	add_submenu_item("Format Date", "date_submenu")

	time_submenu.set_name("time_submenu")
	time_submenu.add_item("12 Hour")
	time_submenu.add_item("24 Hour")
	time_submenu.connect("id_pressed", self, "_on_time_format_pressed")
	add_child(time_submenu)
	add_submenu_item("Format Time", "time_submenu")

	add_separator()
	add_item("Quit")
	var _connection = connect("id_pressed", self, "_on_item_pressed")

func _on_item_pressed(ID):
	match(get_item_text(ID)):
		"Change Font Color":
			$"../ColorPickerPanel".element = "font"
			$"../ColorPickerPanel".visible = true
		"Change Font Outline Color":
			$"../ColorPickerPanel".element = "outline"
			$"../ColorPickerPanel".visible = true
		"Quit":
			get_tree().quit()

func _on_font_pressed(ID):
	var new_family = font_submenu.get_item_text(ID)
	$"../../ClockNode".load_font({"family": families[new_family]})

func _on_font_size_pressed(ID):
	var new_size = int(font_size_submenu.get_item_text(ID).replace("pt", ""))
	$"../../ClockNode".load_font({"size": new_size})

func _on_time_format_pressed(ID):
	match(time_submenu.get_item_text(ID)):
		"12 Hour":
			$"../../ClockNode".hour_format = "12h"
		"24 Hour":
			$"../../ClockNode".hour_format = "24h"
	$"../../ClockNode".save_settings()

func _on_date_format_pressed(ID):
	match(date_submenu.get_item_text(ID)):
		"Sun 31st":
			$"../../ClockNode".date_format = "day date"
			$"../DateLabel".visible = true
		"Sunday, December 31st":
			$"../../ClockNode".date_format = "day month date"
			$"../DateLabel".visible = true
		"12/31/2023":
			$"../../ClockNode".date_format = "MM/DD/YYYY"
			$"../DateLabel".visible = true
		"31/12/2023":
			$"../../ClockNode".date_format = "DD/MM/YYYY"
			$"../DateLabel".visible = true
		"2023/12/31":
			$"../../ClockNode".date_format = "YYYY/MM/DD"
			$"../DateLabel".visible = true
		"None":
			$"../../ClockNode".date_format = "None"
			$"../DateLabel".visible = false
	$"../../ClockNode".save_settings()
